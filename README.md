* Geant4-based Code to test propagation of photons in AIR at sea level (absorption, effects on energies etc...)
* Now set to test effects after 2 km propagation
* Can be easily changed for other particle types and distances.
