clear all
close all
clc

! cat /Home/siv29/dsa030/Desktop/GEANT4/Photon_Atmos_Travel_Distance/build/output/* > fused.txt

yy=importdata('fused.txt');

e_record = yy(:,6);

yy = yy(e_record>50,:);

e_ini = yy(:,3);
event_nb = yy(:,2);
list_e_ini=unique(e_ini);
e_record = yy(:,6);

for ii=1:length(list_e_ini)
    
    tk = list_e_ini(ii)==e_ini;
    
    nb_recorded(ii) = sum(tk);
    nb_sampled(ii) =  max(event_nb(tk));
    
    nb_abs(ii) = 100-nb_recorded(ii)/nb_sampled(ii)*100;
    
    emax(ii) = max(e_record(tk));
    
    list_40MEV = e_record(tk);
    
end

figure(1)
plot([10 list_e_ini'],[100 nb_abs])

ylabel('Absorbed fraction (%)')
xlabel('Energy(keV)')
set(gca,'yscale','log')
set(gca,'xscale','log')
grid on

title('mono energetic photon beams after 2 km propagation')

figure(2)

[n,b] = histcounts(list_40MEV,logspace(log10(50),log10(40000),50));
histogram('binedges',b,'bincounts',n./diff(b),'DisplayStyle','stairs')
set(gca,'yscale','log')
set(gca,'xscale','log')
xlabel('Energy(keV)')
ylabel('counts per bin per bin size (keV-1)')
grid on
title('reponse to 40 MeV photon beam after 2 km propagation')