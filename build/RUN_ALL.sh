energies=(50 100 500 1000 2000 3000 4000 5000 6000)

rm -rf ./output/*

for ener in ${energies[@]}
do
    ./TGF_Propa 1000000 MONO ${ener} &
done