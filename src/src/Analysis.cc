////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include <Settings.hh>
#include <Analysis.hh>
#include <fstream>
#include <iomanip>

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"
#include "G4ios.hh"
#include "G4SteppingManager.hh"
#include "G4ThreeVector.hh"
#include "G4PhysicalConstants.hh"

// singleton pattern initialization
TGFAnalysis *TGFAnalysis::instance = 0;

// constructor
TGFAnalysis::TGFAnalysis()
{

    const G4double ALT_MAX_RECORDED = *std::max_element(Settings::record_altitudes.begin(), Settings::record_altitudes.end());

    const G4String output_filename_second_part =
        std::to_string(Settings::Rand_seed)
        + "_" + std::to_string(int(ALT_MAX_RECORDED))
        + "_" + std::to_string(int(Settings::SOURCE_ALT))
        + "_" + Settings::BEAMING_TYPE
        + "_" + std::to_string(int(Settings::OPENING_ANGLE))
        + "_" + std::to_string(int(Settings::ENERGY)) + ".out";

    asciiFileName2 = "./output/detParticles_" + output_filename_second_part;

    std::ofstream asciiFile00(asciiFileName2, std::ios::trunc); // to clean the output file
    asciiFile00.close();

    output_lines.clear();
}

TGFAnalysis::~TGFAnalysis() {}

TGFAnalysis *TGFAnalysis::getInstance()
{
    if (instance == 0)
        instance = new TGFAnalysis;

    return instance;
}

void TGFAnalysis::save_in_output_buffer(const G4int PDG_NB, const G4double &time, const G4double &energy)
{
    // Write to buffer; only gamma can get here

    if (energy*keV > Settings::MIN_ENERGY_OUTPUT)
    {
        std::stringstream buffer;
        buffer << std::scientific << std::setprecision(6); // scientific notation with
        // 5 significant digits
        //   asciiFile << name;
        //   asciiFile << ' ';
        buffer << Settings::Rand_seed;
        buffer << ' ';
        buffer << Settings::NB_EVENT;
        buffer << ' ';
        buffer << Settings::ENERGY;
        buffer << ' ';
        buffer << PDG_NB;
        buffer << ' ';
        buffer << time;
        buffer << ' ';
        buffer << energy; 
        buffer << G4endl;

        output_lines.push_back(buffer.str());

        write_in_output_file();
    }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void TGFAnalysis::write_in_output_file()
{
    if (output_lines.size() <= output_buffer_size)
        return;

    std::ofstream asciiFile2;
    asciiFile2.open(asciiFileName2, std::ios::app);

    if (asciiFile2.is_open())
    {
        for (G4String &line : output_lines)
        {
            asciiFile2 << line;
        }

        asciiFile2.close();
        output_lines.clear();
    }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void TGFAnalysis::write_in_output_file_endOfRun()
{
    if (output_lines.size() <= 0)
        return;

    std::ofstream asciiFile1;
    asciiFile1.open(asciiFileName2, std::ios::app);

    if (asciiFile1.is_open())
    {
        for (G4String &line : output_lines)
        {
            asciiFile1 << line;
        }

        asciiFile1.close();
        output_lines.clear();
    }
}
