////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include <RunAction.hh>
#include <SteppingAction.hh>
#include "globals.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "RegionInformation.hh"

XrayTelSteppingAction::XrayTelSteppingAction()
{
}

XrayTelSteppingAction::~XrayTelSteppingAction() {}

void XrayTelSteppingAction::UserSteppingAction(const G4Step *aStep)
{

G4StepPoint *thePrePoint = aStep->GetPreStepPoint();
G4StepPoint *thePostPoint = aStep->GetPostStepPoint();

    const G4int PDG = aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

    double time = thePrePoint->GetGlobalTime() / microsecond;
    double energy = thePrePoint->GetKineticEnergy() / keV;

    if (thePrePoint->GetPosition().z() <= 2.0*km && thePostPoint->GetPosition().z() / km > 2.0) {
        analysis->save_in_output_buffer(PDG,time,energy);
        aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    }
}
