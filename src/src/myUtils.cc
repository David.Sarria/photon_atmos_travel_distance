
#include <G4ThreeVector.hh>
#include "myUtils.hh"

namespace myUtils
{

long reversDigits(long num)
{
    long rev_num = 0;
    while (num > 0)
    {
        rev_num = rev_num * 10 + num % 10;
        num = num / 10;
    }
    return rev_num;
}

bool addOvf(long *result, long a, long b)
{
    // adding two long integers but checking if the sum does not overflow
    if (a > std::numeric_limits<long>::max() - b)
        return false;
    else
    {
        *result = a + b;
        return true;
    }
}

std::string convertToString(char a[37], int size) {
        int i;
        std::string s = "";
        for (i = 0; i < size; i++) {
            s = s + a[i];
        }
        return s;
    }

    std::string change_letters_to_numbers(std::string my_str) {
        std::replace(my_str.begin(), my_str.end(), 'a', '1');
        std::replace(my_str.begin(), my_str.end(), 'b', '2');
        std::replace(my_str.begin(), my_str.end(), 'c', '3');
        std::replace(my_str.begin(), my_str.end(), 'd', '4');
        std::replace(my_str.begin(), my_str.end(), 'e', '5');
        std::replace(my_str.begin(), my_str.end(), 'f', '6');
        std::replace(my_str.begin(), my_str.end(), 'g', '7');
        std::replace(my_str.begin(), my_str.end(), 'h', '8');
        std::replace(my_str.begin(), my_str.end(), 'i', '9');
        std::replace(my_str.begin(), my_str.end(), 'j', '1');
        std::replace(my_str.begin(), my_str.end(), 'j', '2');
        std::replace(my_str.begin(), my_str.end(), 'l', '3');
        std::replace(my_str.begin(), my_str.end(), 'm', '4');
        std::replace(my_str.begin(), my_str.end(), 'n', '5');
        std::replace(my_str.begin(), my_str.end(), 'o', '6');
        std::replace(my_str.begin(), my_str.end(), 'p', '7');
        std::replace(my_str.begin(), my_str.end(), 'k', '8');
        std::replace(my_str.begin(), my_str.end(), 'r', '9');
        std::replace(my_str.begin(), my_str.end(), 's', '1');
        std::replace(my_str.begin(), my_str.end(), 't', '2');
        std::replace(my_str.begin(), my_str.end(), 'u', '3');
        std::replace(my_str.begin(), my_str.end(), 'v', '4');
        std::replace(my_str.begin(), my_str.end(), 'w', '5');
        std::replace(my_str.begin(), my_str.end(), 'x', '6');
        std::replace(my_str.begin(), my_str.end(), 'y', '7');
        std::replace(my_str.begin(), my_str.end(), 'z', '8');
        std::replace(my_str.begin(), my_str.end(), '-', '0');
        return my_str;
    }

    int generate_integer(const std::string my_str) {
        int a_int = 0;

        for (int ii = 0; ii < my_str.size(); ++ii) {
            int to_add = (int) my_str[ii] - (int) 48;
            a_int = a_int + to_add * 1000;
        }

        return a_int;
    }

    long generate_a_unique_ID() {

        uuid_t uu;
        uuid_generate(uu);
        char uuid[37];
        uuid_unparse(uu, uuid);
//        std::cout << uuid << std::endl;
        int mys = sizeof(uuid) / sizeof(char);
        std::string my_str = std::string(convertToString(uuid, mys));

        my_str = change_letters_to_numbers(my_str);

        my_str = my_str.substr(1, my_str.size() - 27);
        long output = std::stol(my_str);
//        int dummy = 1 + 1;
        std::cout << output << std::endl;
        return output;
    }

/////////////////////////////////////////////////////////

double get_wall_time()
{
    std::chrono::high_resolution_clock m_clock;
    double time = std::chrono::duration_cast<std::chrono::seconds>(
                      m_clock.now().time_since_epoch())
                      .count();
    return time;
}

}